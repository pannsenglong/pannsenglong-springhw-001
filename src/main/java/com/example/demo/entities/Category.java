package com.example.demo.entities;

import javax.persistence.*;

@Entity
@Table(name = "tb_category")
public class Category {
    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String title;


    public Category(){}

    public Category(int id , String title) {
        this.id = id;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}
