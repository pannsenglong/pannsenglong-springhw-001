package com.example.demo.repositories;

import com.example.demo.entities.Article;
import com.example.demo.entities.Category;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class ArticlesRepository
{
    @PersistenceContext
    EntityManager entityManager;


    @Transactional
    public Article getOne(int id)
    {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Article> criteriaQuery = criteriaBuilder.createQuery(Article.class);
        Root<Article> articleRoot = criteriaQuery.from(Article.class);
        criteriaQuery.where(criteriaBuilder.equal(articleRoot.get("id"), id));
        criteriaQuery.select(articleRoot);
        Article article = entityManager.createQuery(criteriaQuery).getSingleResult();
        System.out.println(article.toString());
        return article;
    }


    @Transactional
    public List<Article> getAll()
    {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Article> criteriaQuery = criteriaBuilder.createQuery(Article.class);
        Root<Article> articleRoot = criteriaQuery.from(Article.class);
        criteriaQuery.select(articleRoot);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }


    @Transactional
    public void insertArticle(Article article)
    {
        if (!isForeignKeyExist(article.getCategory().getId()))
        {
            entityManager.persist(new Category(article.getCategory().getId(), article.getCategory().getTitle()));
            entityManager.flush();
        }
        entityManager.persist(article);
    }


    @Transactional
    public boolean updateArticle(int id, Article article) {
        Query query = entityManager.createQuery("UPDATE Article a SET a.author = :author, a.title = :title," +
                "a.description = :description, a.category.id = :category_id WHERE a.id =:id")
                .setParameter("id",id)
                .setParameter("author",article.getAuthor())
                .setParameter("title",article.getTitle())
                .setParameter("description",article.getDescription())
                .setParameter("category_id",article.getCategory().getId());
        if(query.executeUpdate() <= 0)
            return false;
        return true;
    }


    @Transactional
    public boolean deleteArticle(int id)
    {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaDelete<Article> criteriaQuery = criteriaBuilder.createCriteriaDelete(Article.class);
        Root<Article> articleRoot = criteriaQuery.from(Article.class);
        criteriaQuery.where(criteriaBuilder.equal(articleRoot.get("id"), id));
        Query deleteQuery = entityManager.createQuery(criteriaQuery);
        if(deleteQuery.executeUpdate() <= 0)
            return false;
        return true;
    }


    @Transactional
    public List<Article> getArticleByCategoryTitle(String categoryTitle)

    {
        Query query = entityManager.createQuery("SELECT a FROM Article a JOIN FETCH a.category c WHERE c.title =: categoryTitle")
                .setParameter("categoryTitle",categoryTitle);
        return query.getResultList();
    }



    public boolean isForeignKeyExist(int id){
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Category> criteriaQuery = criteriaBuilder.createQuery(Category.class);
        Root<Category> articleRoot = criteriaQuery.from(Category.class);
        criteriaQuery.where(criteriaBuilder.equal(articleRoot.get("id"), id));
        criteriaQuery.select(articleRoot);
        List<Category> category = entityManager.createQuery(criteriaQuery).getResultList();
        System.out.println(category.toString());
        if (category.size() <= 0)
            return false;
        return true;
    }

}
