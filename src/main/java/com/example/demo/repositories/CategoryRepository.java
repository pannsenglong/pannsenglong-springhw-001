package com.example.demo.repositories;

import com.example.demo.entities.Article;
import com.example.demo.entities.Category;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class CategoryRepository
{
    @PersistenceContext
    EntityManager entityManager;


    @Transactional
    public Category getOne(int id)
    {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Category> criteriaQuery = criteriaBuilder.createQuery(Category.class);
        Root<Category> cateRoot = criteriaQuery.from(Category.class);
        criteriaQuery.where(criteriaBuilder.equal(cateRoot.get("id"), id));
        criteriaQuery.select(cateRoot);
        Category category = entityManager.createQuery(criteriaQuery).getSingleResult();
        return category;
    }



    @Transactional
    public List<Category> getAll()
    {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Category> criteriaQuery = criteriaBuilder.createQuery(Category.class);
        Root<Category> cateRoot = criteriaQuery.from(Category.class);
        criteriaQuery.select(cateRoot);
        List<Category> categories = entityManager.createQuery(criteriaQuery).getResultList();
        return categories;
    }


    @Transactional
    public String insertCategory(Category category)
    {
        if (!isForeignKeyExist(category.getId()))
        {
            entityManager.persist(category);
            entityManager.flush();
            return "Category inserted successfully!";
        }
        return "Category already exist!";
    }


    @Transactional
    public boolean updateCategory(int id, Category category) {
        Query query = entityManager.createQuery("UPDATE Category c SET c.title = :title WHERE c.id = :id")
                .setParameter("title",category.getTitle())
                .setParameter("id",id);
        if(query.executeUpdate() <= 0)
            return false;
        return true;
    }


    @Transactional
    public boolean deleteCategory(int id)
    {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaDelete<Category> criteriaQuery = criteriaBuilder.createCriteriaDelete(Category.class);
        Root<Category> articleRoot = criteriaQuery.from(Category.class);
        criteriaQuery.where(criteriaBuilder.equal(articleRoot.get("id"), id));
        Query deleteQuery = entityManager.createQuery(criteriaQuery);
        if(deleteQuery.executeUpdate() <= 0)
            return false;
        return true;
    }



    @Transactional
    public List<Category> getCategoryTitle(String categoryTitle)
    {
        Query query = entityManager.createQuery("SELECT c FROM Category c WHERE c.title =: categoryTitle")
                .setParameter("categoryTitle",categoryTitle);
        return query.getResultList();
    }


    public boolean isForeignKeyExist(int id){
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Category> criteriaQuery = criteriaBuilder.createQuery(Category.class);
        Root<Category> articleRoot = criteriaQuery.from(Category.class);
        criteriaQuery.where(criteriaBuilder.equal(articleRoot.get("id"), id));
        criteriaQuery.select(articleRoot);
        List<Category> category = entityManager.createQuery(criteriaQuery).getResultList();
        System.out.println(category.toString());
        if (category.size() <= 0)
            return false;
        return true;
    }

}
