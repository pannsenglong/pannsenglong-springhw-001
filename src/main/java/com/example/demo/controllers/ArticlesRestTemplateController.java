package com.example.demo.controllers;

import com.example.demo.entities.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/RestTemplate")
public class ArticlesRestTemplateController
{
    private static String BASE_URL = "http://localhost:8080/articles/";

    @Autowired
    RestTemplate restTemplate;


    //TODO: Get All One Article ==========================================================================
    @GetMapping("articles/{id}")
    public ResponseEntity<Article> getOne(@PathVariable int id)
    {
        return restTemplate.getForEntity(BASE_URL + id, Article.class);
    }



    //TODO: Get All Articles ============================================================================
    @GetMapping("/articles")
    public ResponseEntity<List<Article>> getAll()
    {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> httpEntity = new HttpEntity<String>(headers);
        List<Article> result = restTemplate.exchange(BASE_URL, HttpMethod.GET, httpEntity, List.class).getBody();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }



    //TODO: Insert Article ===============================================================================
    @PostMapping("/articles")
    public ResponseEntity<Article> insertArticle(@RequestBody Article article)
    {
         return restTemplate.postForEntity(BASE_URL,article, Article.class);
    }



    //TODO: Delete Article ===============================================================================
    @DeleteMapping("/articles/{id}")
    public ResponseEntity deleteArticle(@PathVariable int id)
    {
        restTemplate.delete(BASE_URL + id, id);
        return new ResponseEntity("Article deleted successfully!", HttpStatus.OK);
    }



    //TODO: Update Article ===============================================================================
    @PutMapping("/articles/{id}")
    public ResponseEntity updateArticle(@PathVariable int id, @RequestBody Article article)
    {


        restTemplate.put(BASE_URL + id, article);
        return new ResponseEntity(article, HttpStatus.OK);
    }



    //TODO: Get Articles By Category Title=================================================================
    @GetMapping("/articles/category-title")
    public ResponseEntity<List<Article>> getArticleByCategoryTitle(@RequestParam("title") String title)
    {
        List<Article> articles = restTemplate.exchange(BASE_URL + "category-title?title=" + title, HttpMethod.GET, null, List.class).getBody();
        return new ResponseEntity<>(articles, HttpStatus.OK);
    }



}
