package com.example.demo.controllers;

import com.example.demo.entities.Category;
import com.example.demo.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoriesController
{
    CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    //TODO: Get All One Category ==========================================================================
    @GetMapping("/{id}")
    public Category getOne(@PathVariable int id)
    {
        return categoryRepository.getOne(id);
    }


    //TODO: Get All Category ============================================================================
    @GetMapping
    public List<Category> getAll()
    {
        return categoryRepository.getAll();
    }


    //TODO: Insert Category ===============================================================================
    @PostMapping
    public ResponseEntity insertArticle(@RequestBody Category category)
    {
        return new ResponseEntity(categoryRepository.insertCategory(category), HttpStatus.MULTI_STATUS.OK);
    }


    //TODO: Update Category ===============================================================================
    @PutMapping("/{id}")
    public ResponseEntity updateArticle(@PathVariable int id, @RequestBody Category category)
    {
        if(categoryRepository.updateCategory(id, category))
            return new ResponseEntity("Category updated successfully!", HttpStatus.OK);
        return new ResponseEntity("Category failed to update!", HttpStatus.NOT_FOUND);
    }



    //TODO: Delete Category ===============================================================================
    @DeleteMapping("/{id}")
    public ResponseEntity deleteCategory(@PathVariable int id)
    {
        if (categoryRepository.deleteCategory(id))
            return new ResponseEntity("Category delete successfully!", HttpStatus.OK);
        return new ResponseEntity("Category failed to delete!", HttpStatus.NOT_FOUND);
    }



    //TODO: Get Category By Title=================================================================
    @GetMapping("/category-title")
    public List<Category> getCategoryByTitle(@RequestParam("title") String title)
    {
        return categoryRepository.getCategoryTitle(title);
    }
}
