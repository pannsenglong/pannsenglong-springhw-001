package com.example.demo.controllers;

import com.example.demo.entities.Article;
import com.example.demo.repositories.ArticlesRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/articles")
public class ArticleController
{

    ArticlesRepository articlesRepository;


    @Autowired
    public void setArticlesRepository(ArticlesRepository articlesRepository) {
        this.articlesRepository = articlesRepository;
    }



    //TODO: Get All Articles ============================================================================
    @GetMapping
    public List<Article> getAll()
    {
        return articlesRepository.getAll();
    }



    //TODO: Get All One Article ==========================================================================
    @GetMapping("/{id}")
    public Article getOne(@PathVariable int id)
    {
        return articlesRepository.getOne(id);
    }



    //TODO: Insert Article ===============================================================================
    @PostMapping
    public ResponseEntity insertArticle(@RequestBody Article article)
    {
        articlesRepository.insertArticle(article);
        return new ResponseEntity(article, HttpStatus.MULTI_STATUS.OK);
    }



    //TODO: Delete Article ===============================================================================
    @DeleteMapping("/{id}")
    public ResponseEntity deleteArticle(@PathVariable int id)
    {

        if (articlesRepository.deleteArticle(id))
            return new ResponseEntity("Article delete successfully!", HttpStatus.OK);
        return new ResponseEntity("Article failed to delete!", HttpStatus.NOT_FOUND);
    }



    //TODO: Update Article ===============================================================================
    @PutMapping("/{id}")
    public ResponseEntity updateArticle(@PathVariable int id, @RequestBody Article article)
    {
        if(articlesRepository.updateArticle(id, article))
            return new ResponseEntity("Article updated successfully!", HttpStatus.OK);
        return new ResponseEntity("Article failed to update!", HttpStatus.NOT_FOUND);
    }



    //TODO: Get Articles By Category Title=================================================================
    @GetMapping("/category-title")
    public List<Article> getArticleByCategoryTitle(@RequestParam("title") String title)
    {
        return articlesRepository.getArticleByCategoryTitle(title);
    }








}
